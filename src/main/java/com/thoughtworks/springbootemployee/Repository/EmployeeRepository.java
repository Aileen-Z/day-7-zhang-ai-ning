package com.thoughtworks.springbootemployee.Repository;

import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class EmployeeRepository {
    private List<Employee> employees = new ArrayList<>();

    private static AtomicLong ATOMIC_ID = new AtomicLong(0);

    public EmployeeRepository(){
        for(int i = 0; i < 8; i++){
            employees.add(new Employee(ATOMIC_ID.incrementAndGet(), "name"+i, 28, "male", 1000.0, 1L));
        }
        for(int i=0; i < 8; i++){
            employees.add(new Employee(ATOMIC_ID.incrementAndGet(),"name"+(i+8),28,"female",5000.0,2L));
        }
    }

    public Employee create(@RequestBody Employee employee){
        employee.setId(ATOMIC_ID.incrementAndGet());
        employees.add(employee);
        return employee;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public Employee getEmployeeById(Long id){
        return employees.stream()
                .filter(employee -> Objects.equals(id, employee.getId()))
                .findFirst()
                .orElse(null);
    }
    public  List<Employee> getEmployeesByGender(String gender){
        return employees.stream()
                .filter(employee -> Objects.equals(employee.getGender(),gender))
                .collect(Collectors.toList());
    }
    public Employee updateEmployeeAgeAndSalary(Long id,Employee employee){
        Employee updateEmployee = getEmployeeById(id);
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        return updateEmployee;
    }

    public void deleteEmployeeById(Long id){
        employees.removeIf(employee -> Objects.equals(employee.getCompanyId(),id));
    }

    public List<Employee> getEmployeeByPage(Integer page,Integer size){
        return employees.stream()
                .skip((long) (page-1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }


}
