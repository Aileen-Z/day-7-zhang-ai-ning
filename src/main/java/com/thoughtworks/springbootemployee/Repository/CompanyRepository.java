package com.thoughtworks.springbootemployee.Repository;

import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

@Repository
public class CompanyRepository {
    @Resource
    private EmployeeRepository employeeRepository;
    private final List<Company> companies = new ArrayList<>();

    private static final AtomicLong atomicId = new AtomicLong(0);

        public CompanyRepository() {
            companies.add(new Company(atomicId.incrementAndGet(),"Baidu"));
            companies.add(new Company(atomicId.incrementAndGet(),"xiaomi"));
            companies.add(new Company(atomicId.incrementAndGet(),"huawei"));
            companies.add(new Company(atomicId.incrementAndGet(),"Green"));
            companies.add(new Company(atomicId.incrementAndGet(),"Tera"));
            companies.add(new Company(atomicId.incrementAndGet(),"Google"));
        }
        public List<Company> getCompanies(){
            return companies;
        }

    public Company getCompaniesById(Long id) {
        return companies.stream()
                .filter(company -> company.getId().equals(id))
                .findFirst().orElse(null);
    }
    public List<Employee> getEmployeesByCompanyId(Long id){
        return employeeRepository.getEmployees().stream()
                .filter(employee -> Objects.equals(employee.getCompanyId(),id))
                .collect(Collectors.toList());
    }

    public List<Company> getCompaniesByPage(Integer page,Integer size){
            return companies.stream()
                    .skip((long) (page-1) *size).limit(size)
                    .collect(Collectors.toList());
        }

        public Company addCompany(Company company){
            company.setId(atomicId.incrementAndGet());
            companies.add(company);
            return company;
        }

        public void updateCompany(Long id,Company company){
            Company updateCompany = getCompaniesById(id);
            updateCompany.setName(company.getName());
        }

        public void deleteCompany(Long id){
            companies.removeIf(company -> Objects.equals(company.getId(),id));
        }
}


