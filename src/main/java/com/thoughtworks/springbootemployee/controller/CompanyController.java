package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Repository.CompanyRepository;
import com.thoughtworks.springbootemployee.model.Company;
import com.thoughtworks.springbootemployee.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private CompanyRepository companyRepository;

    public CompanyController(CompanyRepository companyRepository){
        this.companyRepository = companyRepository;
    }
    @GetMapping
    public List<Company> getCompanies(){
        return companyRepository.getCompanies();
    }

    @GetMapping("/{id}")
    public Company getCompaniesById(@PathVariable Long id){
        return companyRepository.getCompaniesById(id);
    }

    @GetMapping("/{id}/employees")
    public List<Employee> getEmployeesByCompanyId(@PathVariable Long id){
        return companyRepository.getEmployeesByCompanyId(id);
    }

    @GetMapping(params = {"page","size"})
    public List<Company> getCompaniesByPage(@RequestParam("page")Integer page, @RequestParam("size") Integer size){
        return companyRepository.getCompaniesByPage(page,size);
    }

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public Company addCompany(@RequestBody Company company){
        return companyRepository.addCompany(company);
    }

    @PutMapping("/{id}")
    public void updateCompany(@PathVariable Long id,@RequestBody Company company){
        companyRepository.updateCompany(id, company);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id){
        companyRepository.deleteCompany(id);
    }

}
